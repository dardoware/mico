#include <MIDIUSB.h>


#define BOTON_01_PIN 2
#define BOTON_02_PIN 3
#define BOTON_03_PIN 4

#define BOTON_04_PIN 6
#define BOTON_05_PIN 7
#define BOTON_06_PIN 8
/*
#define BOTON_07_PIN 1
#define BOTON_08_PIN 1
#define BOTON_09_PIN 1
#define BOTON_10_PIN 1
#define BOTON_11_PIN 1
*/

#define POTE_1_PIN A5 
#define POTE_2_PIN A1 
#define POTE_3_PIN A2 
#define POTE_4_PIN A3

#define NUM_POTES 1
#define NUM_BOTONES 6

#define CANAL 9 // 0: Canal MIDI 1

uint8_t potes_pin[NUM_POTES] = {POTE_1_PIN};
uint8_t potes_val[NUM_POTES];
uint8_t potes_antes[NUM_POTES];
uint8_t potes_CC[NUM_POTES]= {7};

uint8_t botones_pin[NUM_BOTONES] = {BOTON_01_PIN,BOTON_02_PIN, BOTON_03_PIN,BOTON_04_PIN,BOTON_05_PIN,BOTON_06_PIN};
boolean botones_val[NUM_BOTONES];
boolean botones_antes[NUM_BOTONES];
uint8_t botones_nota[NUM_BOTONES] = {41,38,46,39,35,44};


void setup()
{
  for (uint8_t i=0; i<NUM_BOTONES; i++) {
    pinMode(botones_pin[i],INPUT_PULLUP);
    botones_val[i]= digitalRead(botones_pin[i]);
    botones_antes[i]= botones_val[i];
  }
  Serial.begin(115200);
  for (uint8_t i = 0; i<NUM_POTES; i++) {
    potes_val[i]= map(analogRead(potes_pin[i]),0,1023,0,127);
    potes_antes[i]=potes_val[i];
  }

  
  Serial.println("It's showtime");
}

void loop()
{
  //pintarPotes();
  pintarBotones();
  
  leerPotes();
  leerBotones();  
}

void leerPotes()
{
  for (uint8_t i=0; i<NUM_POTES; i++)
  {
    potes_val[i]= map(analogRead(potes_pin[i]),0,1023,0,127);
    if ((potes_val[i] > potes_antes[i]+3) || (potes_val[i]< potes_antes[i]-3)) {
      controlChange(CANAL,potes_CC[i],potes_val[i]);
      Serial.print("Ch "); Serial.print(CANAL + 1); Serial.print(" CC "); Serial.print(potes_CC[i]); Serial.print(" ");Serial.println(potes_val[i]);
    }
    
  }
}

void leerBotones(){
  for  (uint8_t i=0; i<NUM_BOTONES; i++)
  {
    botones_val[i]= digitalRead(botones_pin[i]);
    
    if (botones_val[i] != botones_antes[i] ) {
      if (!botones_val[i]) {
         noteOn(CANAL,botones_nota[i],127);
      } else  {
         noteOff(CANAL,botones_nota[i],0);
      }
      MidiUSB.flush();
      botones_antes[i]= botones_val[i];
    }    
  }
}

void pintarPotes()
{
  Serial.print("Potes: ");
  for (uint8_t i=0; i<NUM_POTES; i++)
  {
    Serial.print(potes_val[i]);
    Serial.print("  ");
  }
  Serial.println();
}

void pintarBotones()
{
  Serial.print("Botones: ");
  for (uint8_t i=0; i<NUM_BOTONES; i++)
  {
    Serial.print(botones_val[i]);
    Serial.print(" ");
  }
  Serial.println();
}

void controlChange(byte channel, byte control, byte value) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, value};
  MidiUSB.sendMIDI(event);
}

void noteOn(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOn = {0x09, 0x90 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOn);
}

void noteOff(byte channel, byte pitch, byte velocity) {
  midiEventPacket_t noteOff = {0x08, 0x80 | channel, pitch, velocity};
  MidiUSB.sendMIDI(noteOff);
}
